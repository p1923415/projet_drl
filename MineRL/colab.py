import gym
import minerl
import environment

# Imports Display
from colabgymrender.recorder import Recorder

import logging

# Imports NN
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch import nn
from collections import namedtuple
import random

import skimage
from skimage.transform import resize
from skimage.color import rgb2gray

logging.disable(logging.ERROR) # reduce clutter, remove if something doesn't work to see the error logs.

# Preprocessing wrappers
class SkipFrame(gym.Wrapper):
    def __init__(self, env, skip):
        """Return only every `skip`-th frame"""
        super().__init__(env)
        self._skip = skip

    def step(self, action):
        """Repeat action, and sum reward"""
        total_reward = 0.0
        for i in range(self._skip):
            # Accumulate reward and repeat the same action
            obs, reward, done, info = env.step(action)
            total_reward += reward
            if done:
                break
        return obs, reward, done, info


class GrayScaleObservation(gym.ObservationWrapper):
    def __init__(self, env):
        super().__init__(env)
        obs_shape = self.observation['pov'].shape[:2]
        self.observation['pov'] = Box(low=0, high=255, shape=obs_shape, dtype=np.uint8)

    def permute_orientation(self, observation):
        # permute [H, W, C] array to [C, H, W] tensor
        observation['pov'] = np.transpose(observation['pov'], (2, 0, 1))
        observation['pov'] = torch.tensor(observation['pov'].copy(), dtype=torch.float)
        return observation

    def observation(self, observation):
        observation['pov'] = self.permute_orientation(observation['pov'])
        transform = T.Grayscale()
        observation['pov'] = transform(observation['pov'])
        return observation


class ResizeObservation(gym.ObservationWrapper):
    def __init__(self, env, shape):
        super().__init__(env)
        if isinstance(shape, int):
            self.shape = (shape, shape)
        else:
            self.shape = tuple(shape)

        obs_shape = self.shape + self.observation['pov'].shape[2:]
        self.observation['pov'] = Box(low=0, high=255, shape=obs_shape, dtype=np.uint8)

    def observation(self, observation):
        transforms = T.Compose(
            [T.Resize(self.shape), T.Normalize(0, 255)]
        )
        observation['pov'] = transforms(observation['pov']).squeeze(0)
        return observation

env = gym.make('Mineline-v0')
env = Recorder(env, './video', fps=60)
# env = SkipFrame(env, skip=4)
# env = GrayScaleObservation(env)
# env = ResizeObservation(env, shape=84)
# env = FrameStack(env, num_stack=4)

env.seed(2413)

# Hyperparameters
HIDDEN_SIZE_1 = 32
HIDDEN_SIZE_2 = 64
HIDDEN_SIZE_3 = 128
INPUT_SIZE = 1
OUTPUT_SIZE = len(env.action_space)
ETA = 0.5

EPSILON_MAX = 0.995
EPSILON_MIN = 0.02
EPSILON_DECAY = 0.9999

ETA_MIN = 1*10**-6
ETA_DECAY = 0.01

DISCOUNT = 0.99

NB_EPOCHS_TRAIN = 100
NB_EPOCHS_TEST = 100
BATCH_SIZE = 50
ALPHA = 0.01

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward', 'done'))

class CircularBuffer(object):
    def __init__(self):
        self.size = 100000
        self.memory = []

    def update(self, *args):
        if len(self.memory) > self.size:
            self.memory.pop(0)
        else:
            self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)


class DQN(nn.Module):
    
    def __init__(self, env):
        
        super(DQN, self).__init__()
        self.env = env
        self.model = nn.Sequential(nn.Conv2d(INPUT_SIZE, HIDDEN_SIZE_1, kernel_size=5, stride=2),
                                    nn.ReLU(),
                                    nn.MaxPool2d(2, stride = 2),
                                    nn.Conv2d(HIDDEN_SIZE_1, HIDDEN_SIZE_2, kernel_size = 5, stride=2),
                                    nn.ReLU(),
                                    nn.AdaptiveAvgPool2d(7),
                                    nn.Flatten(),
                                    nn.Linear(7*7*HIDDEN_SIZE_2, HIDDEN_SIZE_3),
                                    nn.ReLU(),
                                    nn.Linear(HIDDEN_SIZE_3, OUTPUT_SIZE),
                                    nn.ReLU())

        self.loss = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=ETA)
        self.parameters = torch.nn.ModuleList(self.model.children())
        self.buffer = CircularBuffer()



def flipCoin(p):
    """
    Re-definition of the util.flipCoin method (Returns True with probability p)
    @p: probability
    """
    r = random.random()
    return r < p

def computeActionFromIndex(index_action):
        """
        Compute the action to take based on the index of the action
        @index: index of the action
        """
        action = env.action_space.noop()
        n = 0
        choice = ''
        for a in action:
          if index_action == n:
            choice = a
          n += 1
        action[choice] = 1
        action = dict(action)
        
        return action


class Model:
    def __init__(self, env):
        self.env = env

        self.policy_network = DQN(env)
        self.target_network = DQN(env)

        self.epsilon = EPSILON_MAX
        self.discount = DISCOUNT
        self.eta = self.policy_network.optimizer.param_groups[0]['lr']

        self.env.steps_done = 0
        self.loss = 0

    def getQValue(self, observation):
        """
        Compute the Q-Value (best action to take) based on the given observation
        @observation: the current state of the environment
        """
        self.env.steps_done += 1

        # Set the model in inference mode
        self.policy_network.model.eval()

        # Initialize action
        action = env.action_space.noop()

        # Do a random action with probability epsilon
        if flipCoin(self.epsilon):
            action = env.action_space.sample()
            action = dict(action)
            for key, value in action.items():
                action[key] = value.item(0)
        else:
            # Disable gradient calculation to compute the Q-value
            with torch.no_grad():
                index_action = self.policy_network.model(torch.FloatTensor(observation))#.to(self.device))
                computeActionFromIndex(index_action)

            # Set the model back in training mode
            self.policy_network.model.train()

        self.epsilon = max(EPSILON_MIN, self.epsilon * EPSILON_DECAY)

        return action

    def train(self, batch):
        """
        Train the model with the given batch
        @batch: the batch of transitions
        """
        states_batch = torch.cat([torch.from_numpy(s.state) for s in batch])
        next_states_batch = torch.cat([torch.from_numpy(s.next_state) for s in batch])
        actions_batch = torch.tensor([b.action for b in batch])
        rewards_batch = torch.tensor([b.reward for b in batch])
        done_batch = torch.FloatTensor([b.done for b in batch])

        # Get the current learning rate
        self.eta = self.policy_network.optimizer.param_groups[0]['lr']
        self.env.steps_done += 1

        # Set the model in inference mode
        self.policy_network.model.eval()

        # Calculate the prediction Q-Values
        predictions = self.policy_network.model(states_batch)
        predictions = predictions.gather(1, actions_batch)
        predictions =  torch.max(predictions, 1)[0]

        # Disable gradient calculation to compute the next Q-values
        with torch.no_grad():
            index_action = self.target_network.model(next_states_batch)
            computeActionFromIndex(index_action)

        # Set the model back in training mode
        self.policy_network.model.train()

        # Calculate the target Q-values using the Bellman equation
        targets = rewards_batch + self.discount * torch.max(index_action.transpose(1,0), 0)[0] * (1 - done_batch)
        # Calculate the loss and update the model
        loss = self.policy_network.loss(predictions, targets)
        self.loss = loss.item()
        self.policy_network.optimizer.zero_grad()
        loss.backward()
        for p in self.policy_network.model.parameters():
            p.grad.data.clamp_(-1, 1)
        self.policy_network.optimizer.step()

    def test(self, observation):
        """
        Test the model with the given observation
        @observation: the current state of the environment
        """
        self.env.steps_done += 1

        # Set the model in inference mode
        self.policy_network.model.eval()

        # Disable gradient calculation to compute the Q-value
        with torch.no_grad():
            index_action = self.policy_network.model(torch.FloatTensor(observation))
            index_action = np.argmax(index_action[0]).item()
            action = computeActionFromIndex(index_action)
        # Set the model back in training mode
        self.policy_network.model.train()

        return action

    def learningRateDecay(self):
        """
        Decay the learning rate
        """
        self.eta = max(ETA_MIN, self.eta * ETA_DECAY)
        for param_group in self.policy_network.optimizer.param_groups:
            param_group['lr'] = self.eta

    def learningRateIncrease(self):
        """
        Increase the learning rate
        """
        self.eta = min(self.eta / 0.2, ETA)
        for param_group in self.policy_network.optimizer.param_groups:
            param_group['lr'] = self.eta

    def softUpdateTargetNetwork(self, alpha):
        """
        Update the target network with the policy network
        @alpha: scalar value to determine the step size
        """
        for target_network_params, policy_network_params in zip(self.target_network.model.parameters(),
                                                                self.policy_network.model.parameters()):
            target_network_params.data.copy_((1.0 - alpha) * target_network_params.data + alpha * policy_network_params.data)

    def hardUpdateTargetNetwork(self):
        """
        Update the target network with the policy network
        """
        self.target_network.model.load_state_dict(self.policy_network.model.state_dict())

    def saveModel(self):
        """
        Save the model in .pth file
        """
        torch.save(self.policy_network.model, "best_model.pth")

    def loadSavedModel(self):
        """
        Load the saved model
        """
        self.policy_network.model = torch.load("best_model.pth")



# MODEL
model = Model(env)

# BUFFER
buffer = CircularBuffer()

def grayScale(state):
    """
    Resizes the image representing the state and returns the simplified state.
    """
    state = (resize(rgb2gray(state), (224,224), mode="reflect") * 255)
    state = state[np.newaxis, np.newaxis, :, :]
    return torch.tensor(state, dtype=torch.float)

def trainModel():
    # PLOT PARAMETERS
    x_epochs = []
    y_rewards = []
    y_losses = []
    y_etas = []
    y_epsilon = []

    # Best reward will be used to save the best model
    best_reward = 0

    print("Training...")

    for epoch in range(0, NB_EPOCHS_TRAIN):

        observation = grayScale(env.reset()['pov'])[0]
        observation = np.expand_dims(observation, 0)
        resized_obs = observation.copy()
        resized_obs.resize(1,1,64,64)
        observation = resized_obs.copy()
 
        total_reward = 0
        nb_interactions = 0
        done = False

        while not done:
            # Compute action
            action = model.getQValue(observation)
            next_observation, reward, done, _ = env.step(action)
            next_observation = grayScale(next_observation['pov'])[0]
            next_observation = np.expand_dims(next_observation, 0)
            resized_next_obs = next_observation.copy()
            resized_next_obs.resize(1,1,64,64)
            next_observation = resized_next_obs.copy()
            
            total_reward += reward
            nb_interactions += 1
            action = list(action.values())
            buffer.update(observation, action, next_observation, reward, done)
 
            observation = next_observation

            # If the episode is done, print the total reward
            if done:
                x_epochs.append(epoch)
                y_rewards.append(total_reward)
                y_losses.append(model.loss)
                y_etas.append(model.policy_network.optimizer.param_groups[0]['lr'])
                y_epsilon.append(model.epsilon)
                print("Episode: {} - Reward: {} - ETA: {} - EPSILON: {}".format(epoch, total_reward, model.eta, model.epsilon))


                # If the total reward is better than the best reward, save the model
                if total_reward > best_reward or total_reward == 500:
                    best_reward = total_reward
                    # model.saveModel()
                    # print("Model saved")

                # If the total reward is above 450, decrease the learning rate to slow the learning down
                if total_reward > 90.0:
                    model.learningRateDecay()
                # If the total reward is under 300 and the learning rate is under the initial learning rate,
                # increase the learning rate to compensate the drop
                elif total_reward < -100.0 and model.eta < ETA:
                    model.learningRateIncrease()
            # If the episode is not done, train the model, compute loss and update the weights
            if len(buffer) > BATCH_SIZE:
                batch = buffer.sample(BATCH_SIZE)
                model.train(batch)
                # model.softUpdateTargetNetwork(ALPHA)

            done = done
        # Hard update weights of the target network after each episode
        model.hardUpdateTargetNetwork()

    model.saveModel()

    # PLOT
    figure, axis = plt.subplots(2, 2)
    axis[0, 0].plot(x_epochs, y_rewards)
    axis[0, 0].set_title("Cumul Recompenses")
    axis[1, 0].plot(x_epochs, y_losses)
    axis[1, 0].set_title("Loss")
    axis[0, 1].plot(x_epochs, y_etas)
    axis[0, 1].set_title("LR")
    axis[1, 1].plot(x_epochs, y_epsilon)
    axis[1, 1].set_title("epsilon")
    plt.show()


def testModel():
    # PLOT PARAMETERS
    x_epochs = []
    y_rewards = []

    model.loadSavedModel()

    print("Testing...")

    for epoch in range(0, NB_EPOCHS_TEST):
        observation = grayScale(env.reset()['pov'])[0]
        observation = np.expand_dims(observation, 0)
        resized_obs = observation.copy()
        resized_obs.resize(1,1,64,64)
        observation = resized_obs.copy()

        total_reward = 0
        nb_interactions = 0
        done = False

        while not done:
            action = model.test(observation)
            next_observation, reward, done, _ = env.step(action)
            next_observation = grayScale(next_observation['pov'])[0]
            next_observation = np.expand_dims(next_observation, 0)
            resized_next_obs = next_observation.copy()
            resized_next_obs.resize(1,1,64,64)
            next_observation = resized_next_obs.copy()
            total_reward += reward
            nb_interactions += 1
            observation = next_observation

            if done:
                x_epochs.append(epoch)
                y_rewards.append(total_reward)

                print("Episode: {} - Reward: {}".format(epoch, total_reward))
                done = True

    # PLOT
    plt.plot(x_epochs, y_rewards)
    plt.title("lr = {} - HL1 = {} - HL2 = {}".format(ETA, HIDDEN_SIZE_1, HIDDEN_SIZE_2))
    plt.show()

trainModel()

# testModel()

env.release()
env.play()

env.close()