import numpy as np
import torch
import random

from DQN import DQN, ETA

EPSILON_MAX = 0.99
EPSILON_MIN = 0.01
EPSILON_DECAY = 0.999

ETA_MIN = 1*10**-6
ETA_DECAY = 0.01

DISCOUNT = 0.99

ACTIONS = {}


def flipCoin(p):
    """
    Re-definition of the util.flipCoin method (Returns True with probability p)
    @p: probability
    """
    r = random.random()
    return r < p


class Model:
    def __init__(self, env, device):
        self.env = env
        self.device = device

        self.policy_network = DQN(env, device)
        self.target_network = DQN(env, device)

        self.epsilon = EPSILON_MAX
        self.discount = DISCOUNT
        self.eta = self.policy_network.optimizer.param_groups[0]['lr']

        self.env.steps_done = 0
        self.loss = 0

    def getQValue(self, observation):
        """
        Compute the Q-Value (best action to take) based on the given observation
        @observation: the current state of the environment
        """
        self.env.steps_done += 1

        # Set the model in inference mode
        self.policy_network.model.eval()

        # Disable gradient calculation to compute the Q-value
        with torch.no_grad():
            indexAction = self.policy_network.model(torch.FloatTensor(observation))#.to(self.device))
            action = self.env.action_space.noop()
            indexAction = np.argmax(indexAction[0]).item()
            index = 0
            res = ''
            for dictAction in action:
              if indexAction == index:
                res = dictAction
              index += 1
            action[res] = 1

        # Set the model back in training mode
        self.policy_network.model.train()

        # Do a random action with probability epsilon
        if flipCoin(self.epsilon):
            # action_random = self.env.action_space.sample()
            # action = self.env.action_space.noop()
            # action = dict(action_random)
            index_action = random.randint(0, len(self.env.action_space)-1)
            index = 0
            res = ''
            for dictAction in action:
              if indexAction == index:
                res = dictAction
              index += 1
            action[res] = 1

        self.epsilon = max(EPSILON_MIN, self.epsilon * EPSILON_DECAY)

        return action

    def train(self, batch):
        """
        Train the model with the given batch
        @batch: the batch of transitions
        """
        #states_batch = torch.tensor(list_states).to(self.device)
        states_batch = torch.cat([torch.from_numpy(s.state) for s in batch])#.to(self.device)
        next_states_batch = torch.cat([torch.from_numpy(s.next_state) for s in batch])#.to(self.device)
        #next_states_batch = torch.tensor([b.next_state for b in batch]).to(self.device)
        actions_batch = torch.tensor([b.action for b in batch])#.to(self.device)
        rewards_batch = torch.tensor([b.reward for b in batch])#.to(self.device)
        done_batch = torch.FloatTensor([b.done for b in batch])#.to(self.device)

        # Get the current learning rate
        self.eta = self.policy_network.optimizer.param_groups[0]['lr']
        self.env.steps_done += 1

        # Set the model in inference mode
        self.policy_network.model.eval()

        # Calculate the prediction Q-Values
        predictions = self.policy_network.model(states_batch)
        predictions = predictions.gather(1, actions_batch)
        predictions =  torch.max(predictions, 1)[0]

        # Disable gradient calculation to compute the next Q-values
        with torch.no_grad():
            index_action = self.target_network.model(next_states_batch)

        # Set the model back in training mode
        self.policy_network.model.train()

        # Calculate the target Q-values using the Bellman equation
        #next_act = torch.from_numpy(np.array(list(next_action.values())))
        targets = rewards_batch + self.discount * torch.max(index_action.transpose(1, 0), 0)[0] * (1 - done_batch)

        # Calculate the loss and update the model
        loss = self.policy_network.loss(predictions, targets)
        self.loss = loss.item()
        self.policy_network.optimizer.zero_grad()
        loss.backward()
        for p in self.policy_network.model.parameters():
            p.grad.data.clamp_(-1, 1)
        self.policy_network.optimizer.step()

    def test(self, observation):
        """
        Test the model with the given observation
        @observation: the current state of the environment
        """
        self.env.steps_done += 1

        # Set the model in inference mode
        self.policy_network.model.eval()

        # Disable gradient calculation to compute the Q-value
        with torch.no_grad():
            indexAction = self.policy_network.model(torch.FloatTensor(observation))#.to(self.device))
            action = self.env.action_space.noop()
            indexAction = np.argmax(indexAction[0]).item()
            index = 0
            res = ''
            for dictAction in action:
              if indexAction == index:
                res = dictAction
              index += 1
            action[res] = 1
        # Set the model back in training mode
        self.policy_network.model.train()

        return action

    def learningRateDecay(self):
        """
        Decay the learning rate
        """
        self.eta = max(ETA_MIN, self.eta * ETA_DECAY)
        for param_group in self.policy_network.optimizer.param_groups:
            param_group['lr'] = self.eta

    def learningRateIncrease(self):
        """
        Increase the learning rate
        """
        self.eta = min(self.eta / 0.2, ETA)
        for param_group in self.policy_network.optimizer.param_groups:
            param_group['lr'] = self.eta

    def softUpdateTargetNetwork(self, alpha):
        """
        Update the target network with the policy network
        @alpha: scalar value to determine the step size
        """
        for target_network_params, policy_network_params in zip(self.target_network.model.parameters(),
                                                                self.policy_network.model.parameters()):
            target_network_params.data.copy_((1.0 - alpha) * target_network_params.data + alpha * policy_network_params.data)

    def hardUpdateTargetNetwork(self):
        """
        Update the target network with the policy network
        """
        self.target_network.model.load_state_dict(self.policy_network.model.state_dict())

    def saveModel(self):
        """
        Save the model in .pth file
        """
        torch.save(self.policy_network.model, "best_model.pth")

    def loadSavedModel(self):
        """
        Load the saved model
        """
        self.policy_network.model = torch.load("best_model.pth")
