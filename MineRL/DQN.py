import torch
from torch import nn

from circularBuffer import CircularBuffer
from main import OUTPUT_SIZE

HIDDEN_SIZE_1 = 64
HIDDEN_SIZE_2 = 32
INPUT_SIZE = 1

ETA = 0.05
num_frames = 1
im_height = 224
im_width = 224

# 64, 50176, 2, 2
# 1, 224, 224, 3
class DQN(nn.Module):
    
    def __init__(self, env, device):
        
        super(DQN, self).__init__()
        self.device = device
        self.env = env
        self.model = nn.Sequential(nn.Conv2d(INPUT_SIZE, 16, kernel_size=5, stride=2),
                      nn.BatchNorm2d(16),
                      nn.ReLU(),
                      nn.Conv2d(16, 32, kernel_size=5, stride=2),
                      nn.BatchNorm2d(32),
                      nn.ReLU(),
                      nn.Conv2d(32, 64, kernel_size=5, stride=2),
                      nn.BatchNorm2d(64),
                      nn.ReLU(),
                      nn.Flatten(),
                      nn.Linear(40000 , OUTPUT_SIZE))#.to(self.device)

        self.loss = nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.model.parameters(), lr=ETA)
        self.parameters = torch.nn.ModuleList(self.model.children())
        self.buffer = CircularBuffer()

    def forward(self, x):
        for f in self.parameters:
            x = f(x)
        return x
