# Imports MineRL
import gym
import minerl
import environment

# Imports Display
from colabgymrender.recorder import Recorder

import logging

# Imports NN
import numpy as np
import matplotlib.pyplot as plt
import torch
from torch import nn
from collections import namedtuple
import random
import skimage
from skimage.transform import resize
from skimage.color import rgb2gray

from Model import *
from DQN import *
from circularBuffer import CircularBuffer

logging.disable(logging.ERROR)

NB_EPOCHS_TRAIN = 10
NB_EPOCHS_TEST = 10
BATCH_SIZE = 50
ALPHA = 0.01

# ENVIRONMENT
env = gym.make('Mineline-v0')
env = Recorder(env, './video', fps=60)

OUTPUT_SIZE = len(env.action_space)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print("Device: ", device)

# MODEL
model = Model(env, device)

# BUFFER
buffer = CircularBuffer()

def grayScale(state, device):
        """
        Resizes the image representing the state and returns the simplified state.
        """
        state = (
            resize(rgb2gray(state), (224,
                                     224), mode="reflect") * 255
        )
        state = state[np.newaxis, np.newaxis, :, :]
        return torch.tensor(state, dtype=torch.float)#.to(device)

def trainModel():
    # PLOT PARAMETERS
    x_epochs = []
    y_rewards = []
    y_losses = []
    y_etas = []
    y_epsilon = []

    # Best reward will be used to save the best model
    best_reward = 0

    print("Training...")

    for epoch in range(0, NB_EPOCHS_TRAIN):

        observation = grayScale(env.reset()['pov'], device)[0]
        observation = np.expand_dims(observation, 0)
 
        total_reward = 0
        nb_interactions = 0
        done = False

        while not done:
            # Compute action
            action = model.getQValue(observation)
            action = dict(action)
            next_observation, reward, done, _ = env.step(action)
            next_observation = grayScale(next_observation['pov'], device)[0]
            next_observation = np.expand_dims(next_observation, 0)
            total_reward += reward
            nb_interactions += 1
            action = list(action.values())
            buffer.update(observation, action, next_observation, reward, done)
 
            observation = next_observation
            # If the episode is done, print the total reward
            # (terminated: True if reached a terminal state, truncated: True if terminated by external event)
            if done:
                x_epochs.append(epoch)
                y_rewards.append(total_reward)
                y_losses.append(model.loss)
                y_etas.append(model.policy_network.optimizer.param_groups[0]['lr'])
                y_epsilon.append(model.epsilon)
                print("Episode: {} - Reward: {} - ETA: {}".format(epoch, total_reward, model.eta))


                # If the total reward is better than the best reward, save the model
                if total_reward > best_reward or total_reward == 500:
                    best_reward = total_reward
                    # model.saveModel()
                    # print("Model saved")

                # If the total reward is above 450, decrease the learning rate to slow the learning down
                if total_reward > 450.0:
                    model.learningRateDecay()
                # If the total reward is under 300 and the learning rate is under the initial learning rate,
                # increase the learning rate to compensate the drop
                elif total_reward < 300.0 and model.eta < ETA:
                    model.learningRateIncrease()
            # If the episode is not done, train the model, compute loss and update the weights
            if len(buffer) > BATCH_SIZE:
                batch = buffer.sample(BATCH_SIZE)
                model.train(batch)
                # model.softUpdateTargetNetwork(ALPHA)

            done = done
        # Hard update weights of the target network after each episode
        model.hardUpdateTargetNetwork()

        model.saveModel()
    # env.close()

    # PLOT
    figure, axis = plt.subplots(2, 2)
    axis[0, 0].plot(x_epochs, y_rewards)
    axis[0, 0].set_title("Cumul Recompenses")
    axis[1, 0].plot(x_epochs, y_losses)
    axis[1, 0].set_title("Loss")
    axis[0, 1].plot(x_epochs, y_etas)
    axis[0, 1].set_title("LR")
    axis[1, 1].plot(x_epochs, y_epsilon)
    axis[1, 1].set_title("epsilon")
    plt.show()


def testModel():
    # PLOT PARAMETERS
    x_epochs = []
    y_rewards = []

    model.loadSavedModel()

    print("Testing...")

    for epoch in range(0, NB_EPOCHS_TEST):
        observation = env.reset()[0]
        total_reward = 0
        nb_interactions = 0
        done = False
        while not done:
            action = model.test(observation)
            next_observation, reward, done, _ = env.step(action)
            total_reward += reward
            nb_interactions += 1
            observation = next_observation

            if done:
                x_epochs.append(epoch)
                y_rewards.append(total_reward)

                print("Episode: {} - Reward: {}".format(epoch, total_reward))
                done = True

    env.release()
    env.close()
    

    # PLOT
    plt.plot(x_epochs, y_rewards)
    plt.title("lr = {} - HL1 = {} - HL2 = {}".format(ETA, HIDDEN_SIZE_1, HIDDEN_SIZE_2))
    plt.show()

if __name__ == "__main__":
    trainModel()
    # testModel()