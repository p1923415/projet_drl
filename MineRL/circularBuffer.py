from collections import namedtuple
import random

Transition = namedtuple('Transition', ('state', 'action', 'next_state', 'reward', 'done'))


class CircularBuffer(object):
    def __init__(self):
        self.size = 100000
        self.memory = []

    def update(self, *args):
        if len(self.memory) > self.size:
            self.memory.pop(0)
        else:
            self.memory.append(Transition(*args))

    def sample(self, batch_size):
        return random.sample(self.memory, batch_size)

    def __len__(self):
        return len(self.memory)