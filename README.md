# Projet DRL CartPole et MineRL

## Description
Ce projet est un projet de Deep Reinforcement Learning (DRL) sur le CartPole de l'API Gym d'OpenAI, ainsi que sur le jeu
Minecraft via la bibliothèque MineRL. Pour plus d'informations sur le projet, se référer au rapport.
Il est important de noter que le code du MineRL (bien que le dossier comporte plusieurs fichiers) est celui contenu dans le fichier
`colab.py`, ce code a été écrit en utilisant un Google Colab, la totalité de celui-ci est donc dans un seul et même fichier.
Vous pourrez tout de même retrouver un contenu quasi-identique dans les autres fichiers du répertoire bien que les modifications finales
ne leur ont pas été apportées, la décision de les laisser a été prise pour une question de lisibilité et de compréhension de la version 
finale. En effet il est tout à fait possible de vous référer à ces fichiers dans un but de compréhension, pour l'utilisation veuillez 
vous référer à la partie Utilisation de ce README.

## Démo
Pour voir les vidéos de démonstration, veuillez vous référer au lien présent dans les annexes du rapport.

## Installation
Ce projet a été réalisé avec Python 3.10 pour le CartPole et Python 3.8 pour MineRL.
Pour installer les dépendances, rendez-vous dans le dossier de la partie souhaitée et exécutez la commande suivante :
```
pip install -r requirements.txt
```
Il est vivement recommandé d'utiliser un environnement virtuel pour éviter les conflits de dépendances.

## Utilisation
Pour lancer la phase d'apprentissage du CartPole ou bien de MineRL veillez à décommenter la ligne `trainModel()` de leur
fichier respectif, puis exécutez le fichier. Pour lancer la phase de test, décommentez la ligne `testModel()` et exécutez le fichier.

Pour lancer le CartPole, rendez-vous dans le dossier `Cart-Pole` et exécutez la commande suivante :
```
python cart_pole.py
```

Pour lancer MineRL, rendez-vous dans le dossier `MineRL` et exécutez la commande suivante :
```
xvfb-run -a python3 colab.py
```

En cas de problème avec l'exécution de la partie MineRL, il est possible de lancer le programme via ce [Google Colab](https://colab.research.google.com/drive/1mIVWTsrEy4xnY4y2MlMScj3gCGCZIROZ?usp=sharing).
Afin d'utiliser ce Colab, il est nécessaire de créer un compte Google et de se connecter à ce compte, ainsi que de
créer un dossier `MineRL` à la racine du drive avec le dossier `environment` dedans, il est aussi possible de ne pas
exécuter la cellule de montage de votre drive et d'importer manuellement le dossier `MineRL` dans le Colab.
À la première exécution, veuillez passer le paramètre `install` à True, afin d'installer les dépendances nécessaires, puis
une fois que la commande `!pip install --upgrade minerl` a été exécutée, redémarrez le Colab et passez le paramètre
`install` à False. Vous pouvez ensuite exécuter toutes les cellules du Colab (sauf celle du montage du drive si vous
importez les environnements manuellement).

## Auteurs
IDIR Nils, JAVEY Paul

## État du projet
Comme mentionné dans le rapport, il est important de noter que la partie MineRL n'est pas terminée/fonctionnelle. En cas
de test de la partie MineRL, vous noterez que le modèle n'est pas capable d'apprendre et qu'il n'y a donc pas de résultats
exploitables.

## Informations complémentaires
Pour plus d'informations sur le projet ou pour toutes autres questions, veuillez contacter les auteurs.
