import gym
import matplotlib.pyplot as plt
import torch

from DQN import HIDDEN_SIZE_2, HIDDEN_SIZE_1, ETA
from Model import Model
from circularBuffer import CircularBuffer

NB_EPOCHS_TRAIN = 1000
NB_EPOCHS_TEST = 100
BATCH_SIZE = 50
ALPHA = 0.01


# ENV & VIDEO
# env = gym.make("CartPole-v1", render_mode="human")
# env = gym.make("CartPole-v1")
env = gym.make("CartPole-v1", render_mode="rgb_array")
env = gym.wrappers.RecordVideo(env, video_folder="./video_folder", name_prefix="test_cart_pole")
# env.action_space.seed(42)
observation, info = env.reset(seed=42)

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print("Device: ", device)


# MODEL
model = Model(env, device)

# BUFFER
buffer = CircularBuffer()


def trainModel():
    # PLOT PARAMETERS
    x_epochs = []
    y_rewards = []
    y_losses = []
    y_etas = []
    y_epsilon = []

    # Best reward will be used to save the best model
    best_reward = 0

    print("Training...")

    for epoch in range(0, NB_EPOCHS_TRAIN):
        observation = env.reset()[0]
        total_reward = 0
        nb_interactions = 0
        done = False

        while not done:
            # Compute action
            action = model.getQValue(observation)
            next_observation, reward, terminated, truncated, _ = env.step(action)
            total_reward += reward
            nb_interactions += 1
            buffer.update(observation, action, next_observation, reward, terminated or truncated)

            observation = next_observation

            # If the episode is done, print the total reward
            # (terminated: True if reached a terminal state, truncated: True if terminated by external event)
            if terminated or truncated:
                x_epochs.append(epoch)
                y_rewards.append(total_reward)
                y_losses.append(model.loss)
                y_etas.append(model.policy_network.optimizer.param_groups[0]['lr'])
                y_epsilon.append(model.epsilon)
                print("Episode: {} - Reward: {} - ETA: {}".format(epoch, total_reward, model.eta))

                # If the total reward is better than the best reward, save the model
                if total_reward > best_reward or total_reward == 500:
                    best_reward = total_reward
                    # model.saveModel()
                    # print("Model saved")

                # If the total reward is above 450, decrease the learning rate to slow the learning down
                if total_reward > 450.0:
                    model.learningRateDecay()
                # If the total reward is under 300 and the learning rate is under the initial learning rate,
                # increase the learning rate to compensate the drop
                elif total_reward < 300.0 and model.eta < ETA:
                    model.learningRateIncrease()

            # If the episode is not done, train the model, compute loss and update the weights
            if len(buffer) > BATCH_SIZE:
                batch = buffer.sample(BATCH_SIZE)
                model.train(batch)
                # model.softUpdateTargetNetwork(ALPHA)

            done = terminated or truncated
        # Hard update weights of the target network after each episode
        model.hardUpdateTargetNetwork()

    model.saveModel()
    env.close()

    # PLOT
    figure, axis = plt.subplots(2, 2)
    axis[0, 0].plot(x_epochs, y_rewards)
    axis[0, 0].set_title("Cumul Recompenses")
    axis[1, 0].plot(x_epochs, y_losses)
    axis[1, 0].set_title("Loss")
    axis[0, 1].plot(x_epochs, y_etas)
    axis[0, 1].set_title("LR")
    axis[1, 1].plot(x_epochs, y_epsilon)
    axis[1, 1].set_title("epsilon")
    plt.show()


def testModel():
    # PLOT PARAMETERS
    x_epochs = []
    y_rewards = []

    model.loadSavedModel()

    print("Testing...")

    for epoch in range(0, NB_EPOCHS_TEST):
        observation = env.reset()[0]
        total_reward = 0
        nb_interactions = 0
        done = False
        while not done:
            action = model.test(observation)
            next_observation, reward, terminated, truncated, _ = env.step(action)
            total_reward += reward
            nb_interactions += 1
            observation = next_observation

            if terminated or truncated:
                x_epochs.append(epoch)
                y_rewards.append(total_reward)

                print("Episode: {} - Reward: {}".format(epoch, total_reward))
                done = True

    env.close()

    # PLOT
    plt.plot(x_epochs, y_rewards)
    plt.title("lr = {} - HL1 = {} - HL2 = {}".format(ETA, HIDDEN_SIZE_1, HIDDEN_SIZE_2))
    plt.show()


if __name__ == "__main__":
    #trainModel()

    testModel()
