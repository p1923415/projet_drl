import torch
from torch import nn

from circularBuffer import CircularBuffer

HIDDEN_SIZE_1 = 64
HIDDEN_SIZE_2 = 32
ETA = 0.05


class DQN(nn.Module):
    def __init__(self, env, device):
        super(DQN, self).__init__()
        self.device = device
        self.env = env
        self.model = nn.Sequential(nn.Linear(env.observation_space.shape[0], HIDDEN_SIZE_1),
                                   nn.ReLU(),
                                   nn.Linear(HIDDEN_SIZE_1, HIDDEN_SIZE_2),
                                   nn.ReLU(),
                                   nn.Linear(HIDDEN_SIZE_2, env.action_space.n)).to(self.device)
        self.loss = nn.MSELoss()
        self.optimizer = torch.optim.SGD(self.model.parameters(), lr=ETA)
        self.parameters = torch.nn.ModuleList(self.model.children())
        self.buffer = CircularBuffer()

    def forward(self, x):
        for f in self.parameters:
            x = f(x)
        return x

    def predict(self):
        observation = self.env.reset()[0]

        for _ in range(1000):
            action = self.model(torch.FloatTensor(observation).to(self.device))
            if action[0] > action[1]:
                action = 0
            else:
                action = 1

            next_observation, reward, terminated, truncated, _ = self.env.step(action)

            if terminated or truncated:
                print("ok")

            observation = next_observation
